import { Map } from './classes/Map'

document.addEventListener('DOMContentLoaded', () => {
  const map = new Map('map')
  map.init()
})

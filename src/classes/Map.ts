import L from 'leaflet'

export class Map {
  constructor(private id: string) {
    this.id = id
  }

  init() {
    const map = L.map(this.id).setView([50, 10], 4)

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map)
  }
}
